# 25.03.2015

 * 10:00 arrival, setup of work environment
 * 12:27 writing proposal
 * 13:03 lunch break
 * 14:21 continue proposal
 * 15:42 build with gurobi
 * 16:01 continue proposal
 * 18:05 end writing proposal

# 26.03.2015

 * 12:00 arrival
 * 12:40 start of work on the proposal
 * 13:40 lunch break
 * 14:03 continue proposal
 * 15:03 blue card
 * 15:23 continue proposal
 * 19:09 end of day at work

# 27.03.2015

 * 10:21 arrival and wasting time with vim and its plugins
 * 12:00 continue proposal
 * 13:00 essen
 * 13:30 continue proposal
 * 15:00 end writing of proposal
 * 16:25 end of break start working on the project

# 30.03.2015

 * 15:51 start of day
 * 21:45 end of day 

# 31.03.2015

 * 15:59 start of day
 * 16:06 logging script works
 * 17:52 finished writing log script
 * 18:00 proposal work
 * 20:29 proposal finished
 * 23:16 exit the day

# 01.04.2015

 * 09:28 stated my day
 * 09:42 pause zu müde
 * 14:09 zurück vom essen
 * 15:51 refactoring plugin
 * 21:26 finished working for today

# 07.04.2015

 * 10:18 arrival
 * 13:00 essen
 * 13:30 zurück
 * 18:56 ende

# 08.04.2015

 * 11:26 start of work with qtcreator on removing all stuff from the plugin class
 * 12:01 Essen
 * 12:41 back
 * 15:19 pause
 * 17:59 gurobi license installed

# 09.04.2015

 * 15:41 start
 * 20:23 have to go want to write shader for grid renderer. should be simply possible.

# 10.04.2015

 * 12:52 start of day
 * 18:29 shader works
 * 19:51 renderer is active on demand

# 14.04.2015

 * 12:04 start of day

# 15.04.2015

 * 11:27 start of day
 * 17:08 going to remove all unnecessary templates
 * 19:33 werde rausgeworfen

# 16.04.2015

 * 11:47 day started working on shell prompt
 * 12:27 finished bash edit
 * 16:43 fuck you cmake
 * 16:44 moved project directory to /local and wasted all my time
 * 18:30 going home nothing really accomplished

# 17.04.2015

 * 10:56 day started
 * 11:06 set font in vimrc
 * 11:19 reading stuff about tcl/tk
 * 11:35 try to contact go-gl members
 * 12:17 yay written to notes script in bashrc
 * 12:51 project doesnt compile with clang++
 * 14:53 explained c++ 11 for the new bachelor student
 * 16:12 wirting renderer for own data type
 * 18:13 porting to range view
 * 18:55 a lot of code changed

# 20.04.2015

 * 09:19 day started
 * 10:04 reading the paper Integer-Grid Maps for Reliable Quad Meshing
 * 10:58 cancel reading paper too confus
 * 12:06 essen
 * 13:12 zurück
 * 15:04 generate quad mesh merged with generate quad mesh full quidance
 * 16:11 doing makecontext now
 * 18:54 async is now working nicely

# 21.04.2015

 * 11:34 untemplate libigm
 * 18:30 still writing at the parametrization wrapper

# 22.04.2015

 * 17:20 yay the wrapper works I do a break now

# 23.04.2015

 * 11:39 go master thesis registration formula
 * 12:36 mensa
 * 13:02 back in action

# 24.04.2015

 * 11:53 started the day with reading away the web
 * 17:53 master

# 29.04.2015

 * 20:47 today was a day full of trying to implement functional code into c++
 * 20:47 it's not worth it

# 04.05.2015

 * 13:19 back from mensa
 * 14:45 it would be nice if libigm can build also in debug
 * 17:02 why is it call `get_index` `get_index_constraint` when it has nothing to do with the index and the index is already used somewhere else
 * 17:10 yay I finally found out what the problem was (just usability not a bug)

# 07.05.2015

 * 20:03 implemented first version of line smoothing on the mesh

# 09.05.2015

 * 23:01 moved line smoothing in another file

# 18.05.2015

 * 14:29 start of day, doing setup of work from home
 * 20:00 it just doesn't work

# 19.05.2015

 * 20:00 it just doesn't work at all, I am able to build, but as soon as I run, I get a segmentation fault

# 20.05.2015

 * 19:06 connection hubs are implemented

# 21.05.2015

 * 15:27 start of day
 * 20:25 going home, was working at igm server of own frontend implementation

# 26.05.2015

 * 11:47 start of day continue to work on the server

# 01.06.2015

 * 14:06 proposal with new times and text

# 08.06.2015

 * 14:53 day started, doing sed for searching in the log
 * 19:59 fuck you undefined vtable fuck you hard

# 01.07.2015

 * 19:56 I had to move to a new computer and the result is that I can't work anymore, because I do not see my quadmesh anymore
 * 20:10 licese management is stupid and prevents you from being productive

# 03.07.2015

 * 19:01 still need to do default connectors in case no information for the directinos is given

# 06.07.2015

 * 16:14 TODO make grid renderer only show primary directions
 * 16:14 TODO make context menu for render node
 * 19:51 diese Klimaanlage nerft echt hart
 * 19:51 dieses scheiß kontextmenü wurde nur deshalb nicht angezeight, weil es im scheiß view mode nicht gelistet war dahin sinde meine 4 Stunden Suche
 * 23:56 it is extremly annoying to compile anything with libIGM on my home computer, because it takes ages, and it keeps me all the time busy keeping track of the task manager, and prevent swapping of the compiler

# 10.07.2015

 * 17:03 A* works now

# 13.07.2015

 * 22:11 integrated A*

# 14.07.2015

 * 13:40 start coding now

# 14.08.2015

 * 15:50 made MAKE_BUTTON macro

# 23.09.2015

 * 17:50 args vieles klappt einfach nicht mehr nach dem refactoring
 * 20:53 lines can now be created with shift click
